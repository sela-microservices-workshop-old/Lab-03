# Microservices Workshop
Lab 03: Create a CI pipeline with Jenkins Blue Ocean

---

## Instructions

 
### Build Jobs:

 - Let's create the CI pipelines for the services below: 
 
   - ui-service
   - sum-service
   - subtraction-service
   - multiplication-service
   - division-service
   
 - Use blueocean to create a new pipeline using "Git" to get sources:

![Image 4](Images/lab03-build-01.png)
 
 - Set the repository url (from your gitlab instance), create the credentials to grant access from jenkins and then create the pipeline:

<img alt="Image 3" src="Images/lab03-build-02.png"  width="75%" height="75%">

 - Configure the job to run in a "node" with the "Slave" label:
 
 ![Image 6](Images/lab03-build-03.png)
 
  - Add a environment variable "DOCKER_USERNAME" with your dockerhub username
 
 - Configure the first stage to run "npm install":

```
$ npm install
```

![Image 7](Images/lab03-build-04.png)

 - Configure the next step to execute the tests in parallel:

```
$ npm run test1
$ npm run test2
$ npm run test3
```

![Image 8](Images/lab03-build-05.png)

 - Configure the next stage to build the docker image:

```
$ docker build -t ${DOCKER_USERNAME}/<service-name>:latest .
```

![Image 9](Images/lab03-build-06.png)

 - In the next step, login to docker hub and push the image:

```
$ docker login -u ${DOCKER_USERNAME} -p <dockerhub-password>
$ docker push ${DOCKER_USERNAME}/<service-name>:latest
```

![Image 10](Images/lab03-build-07.png)

 - Save the pipeline to create the jenkins file:

![Image 11](Images/lab03-build-08.png)
 